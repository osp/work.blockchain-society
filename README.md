The Blockchain & Society Policy Research Lab is hosted by the Institute for Information Law at the University of Amsterdam. The lab study the societal impact of blockchain technologies from a law and policy perspective.

See [blockchain-society.science](https://blockchain-society.science)

The lab asks questions like :  
— How are blockchain applications governed? What internal factors contribute to the success of a blockchain application?  
— How do different societal domains deal with blockchain technologies and their potential disruptive effect?  
— What are the most important regulatory issues around blockchain applications, and what are our policy alternatives?

OSP is working on the visual identity, both for print and screen.