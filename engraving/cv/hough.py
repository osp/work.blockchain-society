import cv2
import numpy as np

for num in range(1,10):
  img = cv2.imread('{}.png'.format(num))
  gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
  edges = cv2.Canny(gray,50,150,apertureSize = 3)

  cv2.imwrite('{}.edges.png'.format(num),edges)

  lines = cv2.HoughLines(edges,1,np.pi/180, 95)

  if lines is not None:
    # print(lines.shape, lines[0], lines[1])

    for line in lines:
      # print(line)
      # rho, theta = line[0]
      for rho,theta in line:
          print(theta)
          a = np.cos(theta)
          b = np.sin(theta)
          x0 = a*rho
          y0 = b*rho
          x1 = int(x0 + 1000*(-b))
          y1 = int(y0 + 1000*(a))
          x2 = int(x0 - 1000*(-b))
          y2 = int(y0 - 1000*(a))

          cv2.line(img,(x1,y1),(x2,y2),(0,0,255),1)

    cv2.imwrite('{}.houghlines.png'.format(num),img)
  else:
    print('found no lines on: {}'.format(num))