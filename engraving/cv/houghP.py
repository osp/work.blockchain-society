import cv2
import numpy as np

for num in range(1,14):
  img = cv2.imread('{}.png'.format(num))
  gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
  edges = cv2.Canny(gray,50,100,apertureSize = 7)
  blur = cv2.GaussianBlur(gray, (21,21), 0)
  edgesblur = cv2.Canny(blur,50,100,apertureSize = 7)

  cv2.imwrite('{}.edges.png'.format(num),edges)
  cv2.imwrite('{}.blur.png'.format(num),blur)
  cv2.imwrite('{}.edges.blur.png'.format(num),edgesblur)

  minLineLength = 30
  maxLineGap = 3
  lines = cv2.HoughLinesP(edges,20,np.pi/180,1,minLineLength,maxLineGap)
  if lines is not None:
    for line in lines:
      print(line[0])
      x1,y1,x2,y2 = line[0]
      cv2.line(img,(x1,y1),(x2,y2),(0,255,0),2)
  else:
    print('Found no lines on: {}'.format(num))
  cv2.imwrite('{}.houghPlines.png'.format(num),img)

  lines = cv2.HoughLinesP(edgesblur,1,np.pi/180,90,minLineLength,maxLineGap)
  if lines is not None:
    for line in lines:
      print(line[0])
      x1,y1,x2,y2 = line[0]
      cv2.line(img,(x1,y1),(x2,y2),(0,255,0),2)
  else:
    print('Found no lines on: {}'.format(num))
  cv2.imwrite('{}.houghPlinesblur.png'.format(num),img)