# https://www.imagemagick.org/Usage/compose/
# https://www.imagemagick.org/Usage/layers/#composition

# convert \
#   -size 1x15 -define gradient:vector=0,0,0,15 gradient:white-gray50 -write mpr:screen +delete \ # Generate a screen and write it to a memory pointer?
#   input.jpg \ # Open input file
#   \( +clone \ # Clone geometry
#     \( +clone -rotate 90 \) \ # Insert file geometry again, but rotated
#     -gravity center -compose Over -layers merge \ # Merge two layers
#     -scale 150% \ # Scale just to be sure?
#     -gravity center -border 20x20 \ # Border around picture, for the waves
#     -tile mpr:screen \ # Tile the stored / generated screen
#     -draw "color 0,0 reset" \ # ?
#     -wave 20x200 \ # transform the screen with the wave
#   \) \
#   -compose MinusSrc -composite \ # Compose on top of each other
#   -fill White +opaque Black \ # Fill all non black with white
#   output-gradient-generated.png # # Store as file

IN=$1;
OUT=$2;

convert \
  -size 1x5 -define gradient:vector=0,0,0,5 gradient:Gray95-Gray40 -black-threshold 80% -write mpr:screen +delete \
  $IN \
  -scale 400% \
  -colorspace Gray \
  -unsharp 0x6+6 \
  \( -clone 0 \
      \( +clone -rotate 90 \) \
      -scale 120% \
      -gravity center -compose Over -layers merge \
      -gravity center -border 20x20 \
      -tile mpr:screen \
      -draw "color 0,0 reset" \
      -wave 1x50 \
      -rotate 45 \
    \) \
    -scale 400% \
    -compose MinusSrc -composite \
    -fill White +opaque Black \
    -scale 25% \
    \
  output-layer-1.png;

convert \
  -size 1x5 -define gradient:vector=0,0,0,4 gradient:Gray70-Black -black-threshold 50% -write mpr:screen +delete \
  $IN \
  -scale 400% \
  -colorspace Gray \
  \( -clone 0 \
      \( +clone -rotate 90 \) \
      -scale 120% \
      -gravity center -compose Over -layers merge \
      -gravity center -border 20x20 \
      -tile mpr:screen \
      -draw "color 0,0 reset" \
      -wave 1x25 \
      -rotate 2 \
    \) \
    -scale 400% \
    -compose MinusSrc -composite \
    -fill White +opaque Black \
    -scale 25% \
    \
  output-layer-2.png;

convert \
  -size 1x5 -fill White -define gradient:vector=0,0,0,4 gradient:Gray30-Black  -write mpr:screen +delete \
  $IN \
  -scale 400% \
  -colorspace Gray \
  \( -clone 0 \
      \( +clone -rotate 90 \) \
      -scale 120% \
      -gravity center -compose Over -layers merge \
      -gravity center -border 20x20 \
      -tile mpr:screen \
      -draw "color 0,0 reset" \
      -wave 2x25 \
      -rotate -70 \
    \) \
    -scale 400% \
    -compose MinusSrc -composite \
    -fill White +opaque Black \
    -scale 25% \
    \
  output-layer-3.png;

convert output-layer-*.png -gravity center -compose Darken -layers merge $OUT;

# convert \
#   -size 1x10 -define gradient:vector=0,5,0,10 gradient:gray60-Black -write mpr:screen +delete \
#   input.jpg \
#   \( +clone \
#     \( +clone -rotate 90 \) \
#     -gravity center -compose Over -layers merge \
#     -scale 150% \
#     -gravity center -border 20x20 \
#     -tile mpr:screen \
#     -draw "color 0,0 reset" \
#     -wave 3x25 \
#     -rotate 10 \
#   \) \
#   -compose MinusSrc -composite \
#   -fill White +opaque Black \
#   output-gradient-generated-2.png